#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
#                                   ContaBot
# Is a boot to control to add an operation from telegram to the app HomeBank
# Initialy uses the XML file used by HomeBank.
# TODO:
# add a simple operation
# list categories and accounts
# show budgets
# alert Budgest
# add payee and categories
#
# wihtout test... like a machou
###############################################################################

import logging
import sys
from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove, ParseMode)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          ConversationHandler)
from datetime import date as Date
from homebank import HomeBank

## Enable loggin
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


class Operation():
    """simply class the basic to generate a operation to be saved in the file XHB.
    The rest of attributes can be added later.
    """
    def __init__(self):
        self.date = Date.today().toordinal()
        self.string = [str(Date.today()),]

    def __str__(self):
        text = 'Date=*{}* Amount=*{}* Account=*{}* Paymode=*{}* Category=*{}* Payee=*{}*'.format(*self.string)
        return text
        
    def toString(self):
        """Generate a string of a subelement to be parse by HomeBank class"""
        ope = '<ope '
        for key in self.__dict__.keys():
            value = str(self.__dict__[key])
            ope = ope + '{}="{}" '.format(key, value)
        operation = ope + '/>'
        return operation


# States
AMOUNT, ACCOUNT, PAYMODE, CATEGORY, PAYEE, OTHER = range(6)

OPERATION = Operation()
MESSAGE = []


if len(sys.argv) == 2:
    _file = sys.argv[1]
    HOMEBANK = HomeBank(_file)
else:
    print('Wrong number of arguments')
    quit()

def matrix(_list):
    """Given a list of element, return a grid or matrix of elements
    """
    matrix = []
    keylist = list(_list)
    while keylist:
        temp_list = []
        for key in range(5):
            if keylist:
                temp_list.append(keylist.pop())
        matrix.append(temp_list)
    return matrix

def clean_up(update, context):
    for e in MESSAGE:
        context.bot.delete_message(chat_id=e.chat.id, message_id=e.message_id)
    
def start(update, context):
    """Every start assing a new instance of a Operation every reply by the user
    function will be sent to the next.
    All the reply of the bot and users will be store in the global MESSAGE to 
    be clean up at the end.
    """
    # if I don't define the variable as 'global', don't save the message.
    global MESSAGE 
    OPERATION = Operation()
    MESSAGE = []
    
    bot = context.bot
    me = bot.send_message(text="Type the amount:", chat_id=update.message.chat_id)
    MESSAGE.append(me)
    return AMOUNT

def amount(update, context):
    """first try to convert the amount to a float, ensurance that
    the input is a float and use the correct decimal separator.
    """
    reply_keyboard = matrix(HOMEBANK.account.keys())
    try:
        amount = float(update.message.text)
        MESSAGE.append(update.message)
        OPERATION.amount = amount
        OPERATION.string.append(amount)
        logger.info("Amount added to operation.")

        me = update.message.reply_text("Select the account:",
                                       reply_to_message_id=False,
                                       reply_markup=ReplyKeyboardMarkup(reply_keyboard,
                                                                   one_time_keyboard=True))
        MESSAGE.append(me)
        return ACCOUNT
    except ValueError:
        me = update.message.reply_text(
            "Decimales se separan por 'Puntos' por ej. '2.5' o '-3.25':"
        )
        MESSAGE.append(me)
        logger.info("Wron amount type")
        return AMOUNT

def account(update, context):
    OPERATION.account = HOMEBANK.account[update.message.text]
    OPERATION.string.append(update.message.text)
    
    logger.info("Account added to operation.")
    MESSAGE.append(update.message)

    reply_keyboard = matrix(HOMEBANK.paymode.keys())

    me = update.message.reply_text("Select the paymode:",
                                   reply_markup=ReplyKeyboardMarkup(reply_keyboard,
                                                                    one_time_keyboard=True))
    MESSAGE.append(me)
    return PAYMODE

def paymode(update, context):
    OPERATION.paymode = HOMEBANK.paymode[update.message.text]
    OPERATION.string.append(update.message.text)
    
    logger.info("Paymode added to operation.")
    MESSAGE.append(update.message)
    
    reply_keyboard = matrix(HOMEBANK.category.keys())
    me = update.message.reply_text("Select the category: ",
                                   reply_markup=ReplyKeyboardMarkup(reply_keyboard,
                                                                    one_time_keyboard=True))
    MESSAGE.append(me)
    return CATEGORY

def category(update, context):
    OPERATION.category = HOMEBANK.category[update.message.text]
    OPERATION.string.append(update.message.text)
    
    logger.info("Category added to operation.")
    MESSAGE.append(update.message)
    
    reply_keyboard = matrix(HOMEBANK.payee.keys())
    me = update.message.reply_text("Select the payee:",
                                   reply_markup=ReplyKeyboardMarkup(reply_keyboard,
                                                                    one_time_keyboard=True))
    MESSAGE.append(me)
    return PAYEE

def payee(update, context):
    OPERATION.payee = HOMEBANK.payee[update.message.text]
    OPERATION.string.append(update.message.text)
    
    logger.info("Payee added to operation.")
    MESSAGE.append(update.message)

    reply_keyboard = [['Finish', 'New one']]
    me = update.message.reply_text("Now what?",
                                   reply_markup=ReplyKeyboardMarkup(reply_keyboard,
                                                                    one_time_keyboard=True))
    MESSAGE.append(me)
    return OTHER

def other(update, context):
    """Depends of the user option start a new conversation or end 
    the last one
    """
    option = update.message.text
    MESSAGE.append(update.message)

    bot = context.bot
    me = bot.send_message(text=str(OPERATION),
                          chat_id=update.message.chat_id,
                          parse_mode=ParseMode.MARKDOWN)

    HOMEBANK.add_operation(OPERATION.toString())

    if option == "Otro":
        clean_up(update, context)
        logger.info("New conversation started.")
        return start(update, context)
    return end(update, context)

def end(update, context):
    """Clean up and END the conversation"""
    clean_up(update, context)
    logger.info("Conversation ended.")
    return ConversationHandler.END

def main():
    """
    """
    # Replace YOUR_TOKEN whit the token your's bot token
    updater = Updater("YOUR_TOKEN", use_context=True)

    dp = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            AMOUNT: [MessageHandler(Filters.all, amount)],
            ACCOUNT: [MessageHandler(Filters.all, account)],
            PAYMODE: [MessageHandler(Filters.all, paymode)],
            CATEGORY: [MessageHandler(Filters.all, category)],
            PAYEE: [MessageHandler(Filters.all, payee)],
            OTHER: [MessageHandler(Filters.all, other)]
        },
        fallbacks=[CommandHandler("end", end)]
    )

    dp.add_handler(conv_handler)
#    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
