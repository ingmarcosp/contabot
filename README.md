# ContaBot

Is a bot to add new transaccion to your HomeBank file. Initialy uses the XML file used by HomeBank.

## Things to consider

It will not mark as "Reconciliate" any transaction, you must check and mark it when you do your personal finance.
Also doesn't create (for now) new categories or payee.
Not all the paymode are aviable, but if you leave a comment I can add it.
This software is as is, wiht no warranties

In the Telegram side, you must to check your bot's privacy options in BotFather, if you put the bot on a group must have Admin rights.

## How it works

You will found the dependencies in requirement.txt, clone the project:

`git clone https://gitlab.com/ingmarcosp/contabot.git`

Get into the folder `contabot` and edit the file `contabot.py` adding YOUR_TOKENT.

Run the file `contabot.py` whit the path to your HomeBank file, there is an example file in the documents so you can try the bot:

`./contabot.py test-files/test.xhb`

Enjoy!

## Done:
 - Start the project
 - add a simple transaction
 - list categories, accounts, payee and payments type.
 - add multiple transactions
 - clean up chat
 - complete translation to English

## TODO:
 - show budgets
 - create categories
 - create payee
