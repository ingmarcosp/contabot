#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
#                                   ContaBot
#
# Its a small interface to controll the HomeBank file
#
# It Will read the accounts, categories and operation and write the new
# operation.
#
# TODO: Create Categories
# TODO: Create Payee
#
###############################################################################
from lxml import etree as ETree

class HomeBank():
    """Interface to control HomeBank files.

    """
    def __init__(self, _file):
        self._file = _file
        self.tree = ETree.parse(self._file)
        self.root = self.tree.getroot()
        self.paymode = {'Credit Card': '1', 'Check': '2', 'Cash': '3',
                        'Transfer': '4', 'Debit Card': '6', 'Direct Charge': '11'}
        self.state = {'Cleared': '1', 'Reconciled': '2', 'Remind': '3'}
        self.account = self.read_accounts()
        self.payee = self.read_payee()
        self.category = self.read_category()

    def add_operation(self, operation):
        """Parse from strings, append a new operation and write to file."""
        new = ETree.fromstring(operation)
        self.root.append(new)
        self.write()

    def write(self):
        """Write dwon to a file the modified HomeBank File """
        self.tree.write(self._file)

    def read_accounts(self):
        """Gets all the account from HomeBank"""
        account = {}
        for _account in self.root.findall("account"):
            name =_account.attrib['name']
            account[name] = _account.attrib['key']
        return account

    def read_payee(self):
        """Gets all the payee from HomeBank"""
        payee = {}
        for _payee in self.root.findall('pay'):
            name = _payee.attrib['name']
            payee[name] = _payee.attrib['key']
        return payee

    def read_category(self):
        category = {}
        for _category in self.root.findall('cat'):
            name = _category.attrib['name']
            category[name] = _category.attrib['key']
        return category

    def get_key(self, _dict, value):
        for key in _dict.keys():
            if _dict[key] == value:
                return key
        return None
