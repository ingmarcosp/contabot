#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
copiando un bot de un tutorial
"""

import logging
import telegram
from telegram.error import NetworkError, Unauthorized
from time import sleep


update_id = None


def main():
    """run bot"""
    global update_id
    # telegram token
    bot = telegram.Bot('714285524:AAEX1ETHpqCmZf_HcaA8o0U5G49fKjU0Xgg')
    bot.deleteWebhook()

    try:
        update_id = bot.get_updates()[0].update_id
    except IndexError:
        update_id = None

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    while True:
        try:
            echo(bot)
        except NetworkError:
            sleep
        except Unauthorized:
            update_id += 1


def echo(bot):
    """echo the message sent by user"""
    global update_id
    for update in bot.get_updates(offset=update_id, timeout=10):
        update_id = update.update_id + 1
        if update.message:
            update.message.reply_text("echo: " + update.message.text)

if __name__ == '__main__':
    main()
